# Q onboarding in 5 minutes

## Install MetaMask

First install the browser add-on 'MetaMask'. After successful installation, add a custom network and enter the Q network configuration details that are documented within [MetaMask tutorial](how_to_install_metamask.md). Finally, create two new accounts so you can later send Q tokens from one to another.

## Get your first Q

If you did not yet use the testnet faucet, open a new browser tab and enter the following URL to call the Q testnet faucet. It will send Q tokens to the address that stands at the end of the URL (replace `0x0AbC123...abc` with your own):

`https://faucet.qtestnet.org/0x0AbC123...abc`  

## Verify Q funding

After some seconds, open MetaMask and select the wallet account you just funded. It should now have received a couple of Q's and should display an according balance.

## Send Q

Within MetaMask, click on "send", then "transfer between my accounts" and select the second wallet account you just created. Finish the transaction and wait for the entered amount of Q to be transferred from account to account. You can verify the transaction within the Q block explorer located at `https://explorer.qtestnet.org`. You can of course send Q to any other wallet address you know.

## Earn rewards

Now open a new tab and head over to `https://hq.qtestnet.org`. If you have MetaMask still activated and connected to Q testnet, you should be browsing the Q decentralised app that allows users to [excercise governance rights](how_to_exercise_governance_rights.md), [earn rewards on Q tokens](how_to_earn_extra_Q_tokens.md) or do some [saving and borrowing with Q DeFi](how_to_obtain_a_loan_against_a_collateral.md).
